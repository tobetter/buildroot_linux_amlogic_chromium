#!/bin/sh

DESTDIR=/usr/bin/chromium-browser

CHROMIUM="chromium-53.0.2785.143"

apt -y update
apt -y install libnss3

mkdir -p ${DESTDIR}
cp -a ${PWD}/${CHROMIUM}/arm64/* ${DESTDIR}/

cat>/etc/chromium.default<<__EOF
DEFAULT_URL=https://webglsamples.org/aquarium/aquarium.html
#DEFAULT_URL=https://bookcase.chromeexperiments.com/
#DEFAULT_URL=https://webglsamples.org/dynamic-cubemap/dynamic-cubemap.html
#DEFAULT_URL=https://webglsamples.org/google-io/2011/1000-objects.html
__EOF

cat>/etc/init.d/chromium<<__EOF
#!/bin/sh

. /etc/chromium.default

start() {
	printf "Starting chrome: "
	[ -d /tmp/chrome ] || mkdir -p /tmp/chrome    
	export GOOGLE_API_KEY="no"
	export GOOGLE_DEFAULT_CLIENT_ID="no"
	export GOOGLE_DEFAULT_CLIENT_SECRET="no"
	export XDG_RUNTIME_DIR=/run/user/0
	${DESTDIR}/chrome --no-sandbox --user-data-dir=/tmp/chrome \
--start-fullscreen \${DEFAULT_URL}
        echo "OK"
}

stop() {
        printf "Stopping chrome: "
        killall chrome
        echo "OK"
}
restart() {
        stop
        start
}

case "\$1" in
  start)
        start
        ;;
  stop)
        stop
        ;;
  restart|reload)
        restart
        ;;
  *)  
        echo "Usage: \$0 {start|stop|restart}"
        exit 1
esac

exit \$?
__EOF

chmod +x /etc/init.d/chromium
